# ModemManager

This repository is a copy of the Alpine 3.12 ModemManager package
(https://git.alpinelinux.org/aports.git) sources using our internal libmbim/libqmi packages.<br>
It exsist to allow for additional patches.

# Build using docker

docker-build.sh and dopcker-runner.sh scripts help to build ModemManager using docker.
Use the below command
```
EVB_INTERNAL_APK_REPOSITORY='<url_of_running_repohoster>' TARGET_ARCH="arm32v7" ./docker_build.sh
```
For example, if the repohoster is running at http://172.17.0.2/ on the local machine, then the command will be,
```
EVB_INTERNAL_APK_REPOSITORY='http://172.17.0.2' TARGET_ARCH="arm32v7" ./docker_build.sh
```
Guide to run local repohoster is [here](https://gitlab.com/evbox/firmware/race/meta/-/wikis/Run-a-local-repohoster)

# Changes

The patch applies to ModemManager-1.14.0 (the currently used version).<br>
It changes the following:

## Fix issue #1: properly setup PDP Context ID (CID) 1, use only CID 1

simple-connect interface: connection_step to include apn in register_in_3gpp_or_cdma_network -> (3GPP)register_in_network<br>
(3GPP)register_in_network queries "+CGDCONT?" -> (maybe delete CID 2/3 -> define CID 1 ->) normal register in network (COPS)<br>
needed because the "LTE attch to network recommendations" describes to do this before COPS command!<br>
Bearer/3GPP interface: cid_selection_3gpp = cint_cid_selection_3gpp -> (maybe delete CID 2/3 -> define CID 1 ->) normal connect bearer (dial_3gpp = dial_3gpp_context_step 4 steps with SWWAN..)<br>
this is the "regular" cid_selection stuff (before connecting the "bearer")<br>

## Fix issue #2: Implement modem connection recovery procedure

Run connection monitor procedure every 30 secs (instead of 5 secs)<br>
checks network registration and IP connection status and recovers when necessary<br>
NW registration status is checked with COPS?<br>
IP connection status is checked with:<br>
1. validate mobile operator assigned actual IP address/mask and gateway
2. check OCPP connection status
3. reachability of DNS server (DNS request for www.google.com if needed)

## Do not disable 3G

After several tests it has been decided to **NOT** disable 3G even though it may lead to
some additional RAT/Cell switching. The reason is that 3G is (still) needed in some cases.

## Miscellaneous

1. Set the right UE EPS mode of operation (PS mode 2). It used to be "CS/PS mode 1 of operation" which is a mix<br>
of Circuit-Switching/Packet-Switching. PS mode 2 is the right UE *data centric* mode.

2. Some minor improvements:
    1. Remove non-supported "AT^SQPORT?" port-probe command (it's a "AT+CPAS" now)
    2. Set compatibility mode (S35modemcompat script has been removed)
    3. remove S35modemcpls script because it is not needed
    4. implemented debug hooks for testing
create/delete the following files for manipulated behaviour:
- /tmp/nwrs_nok       NW registration status is forced to NOT OK
- /tmp/nwrs_ok        NW registration status is forced to OK
- /tmp/ipcs_nok       IP connection status is forced to NOT OK
- /tmp/ipcs_ok        IP connection status is forced to OK
- /tmp/rat_only_4g    Force to register to 4G only
the latter can be used with: AT^SCFG="Radio/Band/4G","0x08000000" to make registration practically impossible

3. Implement meaningful signal quality values. These will be retrieved/shown by webconfig,<br>
so let's make them a good indicator of the signal quality. Currently the following<br>
has been implemented:
```
     Empirically defined 2G quality
      dBm(RSSI)     Quality
      -51(31)           93%
     -111(1)             4% --must-be-odd--> 3%
     -113(0)             1%
     NOTE: >93% never happens
     RULE: 1 + rssi*3

     Empirically defined 3G quality
     RSCP SQual Srxlev EC/n0     Quality
      -70    45    15     -3      98%
     -115     5     5    -14       0%
     Examples:
     -108     -     -    -12      7 + 0 + 0 + 7 = 13%
     -107     6     4    -12      8 + 0 + 0 + 6 = 14% --must-be-odd--> 13%
      -94    21     8  , -10     21 + 5 + 3 + 12 = 41%
     RULE: <RSCP_diff_from_-115> + <Squal_diff_from_5>/4 + <Srxlev_diff_from_5> + <EC/n0_diff_from_-15>*3
     NOTE: squal and srxlev can be 0 (in CONN state) which means value decreases with max 20%
           >98% never happens

     Empirically defined 4G quality
     RSRP RSRQ Srxlev     Quality
      -80   -5     40        100%
     -120  -20     10          0%
     RULE: <RSRP_diff_from_-120> + 3*<RSRQ_diff_from_-20> + <Srxlev_diff_from_10>/2
     Examples:
     -112    -17     12      8 + 9 + 1 = 18%
     -110    -12     18      10 + 24 + 4 = 38%
      -97     -6     27      23 + 42 + 8 = 73% --must-be-even--> 72%
     NOTE: srxlev can be 0 (in CONN state) which mean value decreases with max 15%
```

4. Properly deal with URCs, don't let them fail AT command response parsing.
In other words: prevent things like this: "Couldn't parse ^SWWAN response: '+CIEV:"lsta",20,1,24,11,-87^M ^M ^SWWAN: 1, 1, 2'"
This has been done by reading and ignoring "+PBREADY" with an additional NULL reader
(see sysstart_regex) and the default ciev_received handler now handles basically all "+CIEV" URCs, see mm_3gpp_ciev_regex_get.
NOTE: there is a cinterion specific "+CIEV" URC handler, see plugins/cinterion/mm-broadband-modem-cinterion.c:set_unsolicited_events_handlers
      it is left untouched.

5. Increase SWWAN support AT command timeout from 6 to 18 secs and set assert<br>
we should never use the default bearer AT command set

6. fix ModemManager "no supported IP families" bug<br>
(couldn't load supported IP families: Missing +CGDCONT prefix")
as this can lead to NetworkManager permanently failing/refusing to activate ELS61 connection.<br>
In such a case, you;ll see this in the system log:<br>
(modem-broadband[ttyACM1]: failed to connect 'ELS61': Connection requested IPv4 but IPv4 is unsupported by the modem.<br>
device (ttyACM1): state change: prepare -> failed (reason 'modem-init-failed', sys-iface-state: 'managed'))<br>
