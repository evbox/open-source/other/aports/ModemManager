#!/bin/sh

# This script works around a NetworkManager related issue that was raised in
# DMS-1290 and DMS-1245, see:
# https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues/836
#
# It's meant to be triggered when the user launches an LTE turn-off operation 
# via webconfig for example.
# It attempts to synchronize the states of the modem as seen by NetworkManager
# and ModemManager when the user requests to turn off LTE in webconfig. It will
# query the the state of the radio/rfkill switch and modem from NetworkManager &
# ModemManager and will make sure the the modem is disabled when NetworkManager
# is reporting that the radio state is 'disabled' state since that corresponds
# to user-issued requests. If an in-sync state can't be reached after a
# specific number of attempts, it will exit with an error

set -eu

MAX_DC_ATTEMPTS="5"
MAX_DC_TIME_SECONDS="30"
MAX_DISABLED_STATE_CHECKS="5"
MAX_DISABLING_TIME_SECONDS="5" 

MODEM_SYS_PATH="/sys/devices/platform/ocp/47400000.usb/47401400.usb/musb-hdrc.0/usb1/1-1"

# The space is actually needed to distinguish other properties
PROPERTY_MODEM_STATE="modem.generic.state "
PROPERTY_MODEM_POWER_STATE="modem.generic.power-state "

modem_state="unknown"
wwan_status="unknown"
modem_power_state="uknown"

e_info()
{
	echo "INFO: ${*}"
}

e_err()
{
	echo >&2 "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

trim_string()
{
	trim=${1#${1%%[![:space:]]*}}
	trim=${trim%${trim##*[![:space:]]}}

	printf '%s' "${trim}"
}

query_modem_state()
{
	_modem_state_key_value="$(mmcli \
	                                --modem "${MODEM_SYS_PATH}" \
	                                --output-keyvalue \
	                                | grep "^${PROPERTY_MODEM_STATE}" )"
	modem_state="$(echo "${_modem_state_key_value}" | cut -d ":" -f 2 )"
	modem_state="$(trim_string "${modem_state}")"
}

query_modem_power_state()
{
	_modem_power_state_key_value="$(mmcli \
	                                --modem "${MODEM_SYS_PATH}" \
	                                --output-keyvalue \
	                                | grep "^${PROPERTY_MODEM_POWER_STATE}" )"
	modem_power_state="$(echo "${_modem_power_state_key_value}" | cut -d ":" -f 2 )"
	modem_power_state="$(trim_string "${modem_power_state}")"
}

query_wwan_status()
{
	wwan_status="$(nmcli --terse radio wwan)"
}

print_overall_status()
{
	query_modem_state
	query_wwan_status

	e_info "modem state is '${modem_state}'"
	e_info "Wwan status is '${wwan_status}'"
}

sync_nm_mm_disabled_state()
{
	query_wwan_status
	if [ "${wwan_status}" = "disabled" ]; then
		# make sure modem state is past 'disabling/disconnecting' state
		query_modem_state
		_time_in_disabling_state="0"
		while [ "${modem_state}" = "disabling" ] || 
			  [ "${modem_state}" = "disconnecting" ]; do 
			e_info "modem is still in '${modem_state}' state; waiting ..."
			sleep "1"

			_time_in_disabling_state=$((_time_in_disabling_state + 1))
			if [ "${_time_in_disabling_state}" -gt "${MAX_DISABLING_TIME_SECONDS}" ]; then
				e_err "stuck in disabling state for '${MAX_DISABLING_TIME_SECONDS}' seconds"
				break
			fi
			query_modem_state
		done
		
		# if the modem is still not 'disabled', we trigger the action via mmcli
		# directly
		query_wwan_status
		query_modem_state
		if [ "${wwan_status}" = "disabled" ] &&
		   [ "${modem_state}" != "disabled" ]; then
			# make sure we give the original NM 'Turn off' request an adequate
			# amount of time to finish its operation before we issue ours if
			# needed
			_disabled_state_check_count="0"
			while [ "${modem_state}" != "disabled" ]; do
				e_info "modem is not yet disabled, COUNT='${_disabled_state_check_count}'"

				_disabled_state_check_count="$((_disabled_state_check_count + 1))"
				if [ "${_disabled_state_check_count}" -gt "${MAX_DISABLED_STATE_CHECKS}" ]; then
					break;
				fi

				sleep "1"
				query_modem_state
			done

			# the previous loop might still break if the modem reaches 'disabled'
			# state between checks -> only issue the request if modem state is
			# still != 'disabled' 
			_dc_attempts_count="0"
			while [ "${modem_state}" != "disabled" ] && 
			      [ "${_dc_attempts_count}" -lt "${MAX_DC_ATTEMPTS}" ]; do
				e_info "disabling modem via mmcli, attempt '${_dc_attempts_count}'"
				if ! mmcli -m "${MODEM_SYS_PATH}" -d ; then
					e_warn "couldn't issue disable request to modem, retrying ..."
					_dc_attempts_count="$((_dc_attempts_count + 1))"
				else
					e_info "disable request issued successfully"
					e_info "disconnection attempts = '${_dc_attempts_count}'"
					break
				fi
			done
			# ideally _dc_attempts_count should always remain '0' however, in
			# practice, it seems that 2 attempts are sometimes needed
			if [ "${_dc_attempts_count}" -ge "${MAX_DC_ATTEMPTS}" ]; then
				e_err "exceeded '${MAX_DC_ATTEMPTS}' disconnection attempts!"
				print_overall_status
				exit 1
			fi
			
			query_modem_power_state
			if [ "${modem_power_state}" != "low" ]; then
				if ! mmcli -m "${MODEM_SYS_PATH}" --set-power-state-low; then
					e_err "failed to put modem in low-power state"
				fi
			fi
		fi

		# sanity-check, make sure that the modem has reached
		# disabled state and is in low-power mode		
		_time_waiting_for_disable="0"
		while true; do
			query_modem_power_state
			if [ "${modem_power_state}" != "low" ]; then
				e_err "modem is not in low-power mode!"
			fi
			query_modem_state
			if [ "${modem_state}" = "disabled" ] || \
			   [ "${modem_state}" = "disconnected" ] ; then
				break
			fi
			sleep 10
			_time_waiting_for_disable="$((_time_waiting_for_disable + 10))"
			if [ "${_time_waiting_for_disable}" -gt "${MAX_DC_TIME_SECONDS}" ]; then
				e_err "exceeded max time of '${MAX_DC_TIME_SECONDS}' seconds to reach disabled state!"
				print_overall_status
				exit 1
			fi
		done
	fi
	
	print_overall_status
	exit 0
}

main()
{
	sync_nm_mm_disabled_state
}


main

exit 0
